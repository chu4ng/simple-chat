package main

import (
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

type Config struct {
	Server struct {
		Port int32  `yaml:"port"`
		Host string `yaml:"host"`
	} `yaml:"server"`
	Database struct {
		Username string `yaml:"username"`
		Password string `yaml:"password"`
		Hostname string `yaml:"hostname"`
	} `yaml:"database"`
	Misc struct {
		Timezone   int `yaml:"timezone"`
		MaxMsgSize int `yaml:"maxmsgsize"`
		NumOldMsgs int `yaml:"numoldmsgs"`
	} `yaml:"misc"`
}

func readConfig() Config {
	f, err := os.Open("config.yml")
	if err != nil {
		log.Fatalf("Can't open config file: %e", err)
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			log.Fatalf("Can't close config file: %e", err)
		}
	}(f)

	cfg := Config{}
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		log.Fatalf("Can't parse config file: %e", err)
	}
	return cfg
}
