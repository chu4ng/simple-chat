package main

import (
	"bufio"
	"bytes"
	"fmt"
	"github.com/vmihailenco/msgpack/v5"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

const (
	connHost = "localhost"
	connPort = "8080"
	connType = "tcp"
)

type Message struct {
	Text  string `db:"body"`
	Name  string `db:"name"`
	Board string `db:"board"`
	Time  int64  `db:"time"`
	Id    int64  `db:"id" msgpacl:"-"`
}

type Client struct {
	name  string
	board string
}

var (
	w *bufio.Writer
	r *bufio.Reader
)
var client = Client{"anon", "main"}
var messageQueen []Message

// TODO make fucking refactoring, there's too much shit.

func main() {
	fmt.Println("Connecting to " + connType + " server " + connHost + ":" + connPort)
	conn, err := net.Dial(connType, connHost+":"+connPort)
	r = bufio.NewReader(conn)
	w = bufio.NewWriter(conn)
	if err != nil {
		fmt.Println("Error connecting:", err.Error())
		os.Exit(1)
	}
	go receiveMessage()
	for {
		processInput()
	}
}

func (m *Message) pack() (b []byte, err error) {
	m.Time = time.Now().Unix()
	b, err = msgpack.Marshal(m)
	return
}

func printQ() {
	for _, m := range messageQueen {
		if len(m.Text) > 0 {
			m.log()
		}
		if m.Board != client.board {
			continue
		}
	}
	// TODO add sync and waiters?
	messageQueen = []Message{}
}

func receiveMessage() {
	for {
		buffer, err := r.ReadBytes('\n')
		if err != nil {
			log.Fatalf("Can't receive data from server: %s\n", err.Error())
		}
		appendMessageToQueue(buffer)
	}
}

func appendMessageToQueue(buffer []byte) {
	m := Message{}
	buffer = bytes.TrimSuffix(buffer, []byte("\n"))
	err := msgpack.Unmarshal(buffer, &m)
	if err != nil {
		log.Fatalf("Can't unmarchal data: %s\nData is: %s", err.Error(), string(buffer))
	}
	messageQueen = append(messageQueen, m)
}

func (m *Message) log() {
	if m.Name == "" {
		m.Name = "anon"
	}
	hh, mm, ss := time.Unix(m.Time, 0).Clock()
	fmt.Printf("%d\t", m.Id)
	fmt.Printf("%d:%d:%d ", hh, mm, ss)
	fmt.Printf("(%s) : %s: %s\n", m.Board, m.Name, m.Text)
}

func processInput() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Text to send: ")
	textInput, _ := reader.ReadString('\n')
	textInput = strings.TrimSuffix(textInput, "\n")
	if handleCommands(textInput) {
		return
	}
	sendMessage(textInput)
}

func sendMessage(textInput string) {
	mess := &Message{Text: textInput, Name: client.name, Board: client.board}
	b, err := mess.pack()

	if err != nil {
		log.Fatal("Can't marshal message", err)
	}
	b = append(b, '\n')
	_, err = w.Write(b)
	if err != nil {
		log.Fatal("Can't write the message to buffer", err)
	}
	err = w.Flush()
	if err != nil {
		log.Fatal("Can't flush the message", err)
	}
}

func handleCommands(textInput string) bool {
	var buf string
	if strings.HasPrefix(textInput, "/name") {
		buf = strings.TrimPrefix(textInput, "/name")
		client.name = strings.TrimSpace(buf)
		return true
	} else if strings.HasPrefix(textInput, "/board") {
		client.board = strings.TrimPrefix(textInput, "/board")
		return true
	} else if textInput == "/help" {
		println("'/name newname' for changing name")
		println("'/board newshitplace' for changing board")
		return true
	} else if textInput == "" {
		printQ()
		return true
	}
	return false
}
