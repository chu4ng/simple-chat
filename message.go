package main

import (
	"bytes"
	"fmt"
	"time"

	"github.com/vmihailenco/msgpack/v5"
)

type Message struct {
	Text     string `db:"body"`
	Name     string `db:"name"`
	Board    string `db:"board"`
	Time     int64  `db:"time"`
	Id       int64  `db:"id"`
	Command  int64
	Mine     bool
	Datetime string
	sender   *Client `msgpack:",omit"`
}

const (
	MESSAGE           = 0
	UPDATE_ONLINE     = 1
	GET_OLD_MESSAGES  = 2
	GET_MESSAGE_BY_ID = 3
)

func (m *Message) EncodeMsg() ([]byte, error) {
	var buf bytes.Buffer
	enc := msgpack.NewEncoder(&buf)
	enc.UseCompactFloats(true)
	enc.UseCompactInts(true)
	err := enc.Encode(m)
	return buf.Bytes(), err
}

func DecodeMsg(b []byte) (*Message, error) {
	var m Message
	b = bytes.TrimSuffix(b, []byte("\n"))
	err := msgpack.Unmarshal(b, &m)
	return &m, err
}

func (m *Message) log() {
	hh, mm, ss := time.Unix(m.Time, 0).Clock()
	fmt.Printf("%d  ", m.Id)
	fmt.Printf("%02d:%02d:%02d ", hh, mm, ss)
	if m.sender != nil {
		fmt.Printf("(%s) : %s(%s): %s\n", m.Board, m.Name, m.sender.ip, m.Text)
	} else {
		fmt.Printf("(%s) : %s: %s\n", m.Board, m.Name, m.Text)
	}
}
