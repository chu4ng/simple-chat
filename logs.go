package main

import (
	"html/template"
	"log"
	"net/http"
	"time"
)

type tmplData struct {
	Msgs []Message
}

const ONE_DAY = 86400 // in seconds

func handleLogs(w http.ResponseWriter, r *http.Request) {
	date := r.URL.Path[len("/logs/"):]
	if len(date) < 5 {
		t := time.Now().Format("2006-01-02")
		http.Redirect(w, r, "/logs/"+t, http.StatusSeeOther)
		return
	}
	messages, err := getMessagesByDate(date)
	if err != nil {
		log.Printf("Can't reach log msgs for date %s", date)
		return
	}
	sendLogContent(w, messages)
}

func renderTemplate(w http.ResponseWriter, msgs *tmplData) {
	t, err := template.ParseFiles("static/logs.html")
	if err != nil {
		log.Fatalf("Can't open tmpl file static/logs.html: %e", err)
	}
	err = t.Execute(w, msgs)
	if err != nil {
		log.Printf("Can't render log %e", err)
	}
}
func sendLogContent(w http.ResponseWriter, messages []Message) {
	data := &tmplData{messages}
	renderTemplate(w, data)
}

// GET /logs/2022-02-07

func getMessagesByDate(date string) (messages []Message, err error) {
	t, err := time.Parse("2006-01-02", date)
	if err != nil {
		log.Printf("Can't parse %s: %e", date, err)
		return
	}
	startOfDay := t.Unix() + int64(int(time.Hour.Seconds())*cfg.Misc.Timezone)
	endOfDay := startOfDay + ONE_DAY
	messages, err = db.getMessagesByDate(startOfDay, endOfDay)
	return
}
