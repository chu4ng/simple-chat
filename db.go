package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

var schema = `
	CREATE TABLE if not exists messages ( 
    id int generated always as identity,
    body text not null,               
    name text,
    board text not null,              
    time integer NOT NULL
)`

type Database struct {
	inst *sqlx.DB
	conn *sqlx.Conn
}

func initDB() *Database {
	db := Database{}
	var err error
	dbCredentials := fmt.Sprintf("host='%s' user='%s' password='%s' sslmode='disable'", cfg.Database.Hostname, cfg.Database.Username, cfg.Database.Password)

	db.inst, err = sqlx.Connect("postgres", dbCredentials)
	if err != nil {
		// TODO return err statement instead of handle it on the place
		log.Fatalf("Can't connect to database(sqlx): %e", err)
	}

	db.conn, err = db.inst.Connx(context.Background())
	if err != nil {
		log.Fatalf("Can't connect to database(inst): %e", err)
	}

	db.inst.MustExec(schema)

	return &db
}

func (db *Database) getMessageById(id int64) (message Message) {
	var messages []Message
	var err error
	err = db.inst.Select(&messages, "select * from messages where id=$1", id)
	if err != nil {
		log.Printf("Can't reach old messages: %e", err)
	}
	if len(messages) > 0 {
		return messages[0]
	} else {
		return Message{}
	}
}

func (db *Database) getOldMessages(count int, start int64) (messages []Message) {
	messages = []Message{}
	var err error
	if start == 0 {
		// Is that queries're expoitable?
		err = db.inst.Select(&messages, "select * from (select * from messages order by id desc limit $1) ord order by id asc;", count)
	} else {
		err = db.inst.Select(&messages, "select * from (select * from messages where id < $2 order by id desc limit $1) ord order by id asc;", count, int(start))
	}
	if err != nil {
		log.Printf("Can't reach old messages: %e", err)
	}
	return
}

func (db *Database) saveMessage(m *Message) (int64, error) {
	var id int64
	var err error
	if m.Text == "" {
		log.Print("Message is empty, continue")
		err := errors.New("empty message")
		return 0, err
	}
	err = db.inst.QueryRow(
		"INSERT into messages (body, name, board, time) values ($1, $2, $3, $4) RETURNING id",
		m.Text, m.Name, m.Board, m.Time).Scan(&id)
	return id, err
}

func (db *Database) getMessagesByDate(sof, eod int64) (messages []Message, err error) {
	messages = []Message{}
	err = db.inst.Select(&messages, "select * from messages where time > $1 and time < $2 order by id asc;", sof, eod)
	return
}
